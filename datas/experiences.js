let experiences = [

    /**
     * Experience type
     */
    {
        titlejob: {
            lang_fr: "Nom du poste",
            lang_en: "Title name",
            lang_es: "No espanol data"
        },
        enterprise: {
            lang_fr: "Entreprise X",
            lang_en: "Enterprise X",
            lang_es: "No espanol data"
        },
        dates: {
            start: "04/2016",
            end: undefined // or another date (if not specified it will be displayed at 'actual')
        },
        content: {
            lang_fr: [
                "Ligne 1",
                "Ligne 2",
                "Ligne 3",
            ],
            lang_en: [
                "Line 1",
                "Line 2",
                "Line 3",
            ],
            lang_es: [
                "No espanol data",
                "No espanol data",
                "No espanol data",
            ],
        }
    },

];