let profil = {

    displayname: "BobBi",
    photo: "https://shortcut-test2.s3.amazonaws.com/uploads/role/attachment/179147/default_Obi-Wan_Kenobi.png",
    gravatar: false,
    tel: "+00 000 000 000",
    mail: "bo@bi.fr",

    jobtitle: {
        lang_fr: "Travail 1",
        lang_en: "Job 1",
        lang_es: "??? ?",
    },

    resume: {
        lang_fr: `
        <p>Présentation en HTML</p>
        <ul>
            <li>Ligne 1</li>
            <li>Ligne 2</li>
            <li>Ligne 3</li>
        </ul>
        `,
        lang_en: `
        <p>Describe in HTML</p>
        <ul>
            <li>Line 1</li>
            <li>Line 2</li>
            <li>Line 3</li>
        </ul>
        `,
        lang_es: `
        <p>Descripción en HTML</p>
        <ul>
            <li>Linéa 1</li>
            <li>Linéa 2</li>
            <li>Linéa 3</li>
        </ul>
        `
    },


    cv: {
        link: {
            lang_fr:"http://www.doyoubuzz.com/thomas-depretz",
            lang_en:"http://www.doyoubuzz.com/thomas-depretz",
            lang_es:"http://www.doyoubuzz.com/thomas-depretz"
        },        label: {
            lang_fr: "Télécharger mon CV",
            lang_en: "Download my CV",
            lang_es: "???",
        }
    },

    web: {
        link: "http://bobi.fr",
        label: "www.bobi.fr"
    },

    twitter: {
        link: "https://twitter.com/bobi",
        label: "@bobi"
    },

    linkedin: {
        link: "https://fr.linkedin.com/in/bobi",
        label: "Bo BI"
    },

    lang: [
        {
            name: "fr",
            level: 5,
            label: {
                lang_fr: "Français",
                lang_en: "French",
                lang_es: "No espanol data"
            },
        },
        {
            name: "en",
            level: 4,
            label: {
                lang_fr: "Anglais",
                lang_en: "English",
                lang_es: "No espanol data"
            }
        }
    ],

    hobbits: {

        lang_fr:[
            "Mangas",
            "Nouvelles technologies",
        ],
        lang_en:[
            "Mangas",
            "New technologies",
        ],
        lang_es:[
            "No espanol data",
            "No espanol data",
        ],
    }

};