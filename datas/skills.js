let skills = [
    {
        label: {
            lang_fr:"Comp 1",
            lang_en:"Skill 1",
            lang_es:"???? 1"
        },
        level: 2.0
    },
    {
        label: {
            lang_fr:"Comp 2",
            lang_en:"Skill 2",
            lang_es:"???? 2"
        },
        level: 2.5
    },
    {
        label: {
            lang_fr:"Comp 3",
            lang_en:"Skill 3",
            lang_es:"???? 3"
        },
        level: 5.0
    }
];