let formations = [

    /**
     * Exemple type
     */
    {
        title: {
            lang_fr: "Nom du poste",
            lang_en: "Title name",
            lang_es: "No espanol data",
        },
        school: {
            lang_en:"Entreprise X",
            lang_fr:"Enterprise X",
            lang_es:"No espanol data"
        },
        dates: {
            start: "2013",
            end: "2014"
        }, 
        schoolURL : "http://enterprise.x",
        validation: "http://www.rncp.cncp.gouv.fr/grand-public/visualisationFiche?format=fr&fiche=17108"
    },

];