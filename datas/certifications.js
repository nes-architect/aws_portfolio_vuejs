let certifications = [

    {
        label: "AWS Cloud Practionner",
        code: "https://www.certmetrics.com/amazon/public/badge.aspx?i=9&t=c&d=2018-01-12&ci=AWS00401899",
        badge:"https://www.certmetrics.com/api/ob/image/amazon/c/9",
        status: 1,
        date: "12 Janv 2017",
        serial: {
            id: "KMLSPKFK2214189T",
            validationURL: "https://aw.certmetrics.com/amazon/public/verification.aspx"
        } 
    },

    {
        label: "AWS Soltion Architect : Associate",
        code: "",
        badge: "https://www.pjlewis.org/wp-content/uploads/2017/09/aws-solutions-architect-associate.png?x30801&x30801",
        status: 0,
        date: undefined,
        serial: {
            id: undefined,
            validationURL: undefined
        } 
    }

];