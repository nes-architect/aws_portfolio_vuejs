let config = {

    langs: {
        available: ["fr","en","es"],
        default: "en"
    },

    copyright: "Copyright © Thomas DEPRETZ - all rights reserved",

    page_text : {

        resume: {
            lang_fr: "Présentation",
            lang_en: "Resume",
            lang_es: "No espanol data"
        },
        experiences: {
            lang_fr: "Expériences",
            lang_en: "Experiences",
            lang_es: "No espanol data"
        },
        formations: {
            lang_fr: "Diplômes et formations",
            lang_en: "Formations",
            lang_es: "No espanol data"
        },
        certifications: {
            lang_fr: "Certifications",
            lang_en: "Certifications",
            lang_es: "No espanol data"
        },
        skills: {
            lang_fr: "Compétences",
            lang_en: "Skills",
            lang_es: "No espanol data"
        },
        languages: {
            lang_fr: "Langues",
            lang_en: "Languages",
            lang_es: "No espanol data"
        },
        hobbits: {
            lang_fr: "Hobits",
            lang_en: "Hobbits",
            lang_es: "No espanol data"
        }

    }

}