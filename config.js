let config = {

	/**
	* Define all profil settings
	**/
	profil: {

		displayname: "Thomas DEPRETZ",
		jobtitle: "I.T Architect",
		photo: "https://shortcut-test2.s3.amazonaws.com/uploads/role/attachment/179147/default_Obi-Wan_Kenobi.png",
		gravatar: true,

		tel: "+33 621 704 307",
		mail: "thomas@depretz.fr",

		cv: {
			link: "http://www.doyoubuzz.com/thomas-depretz",
			label: "Download CV on DoYouBuzz"
		},

		web: {
			link: "http://www.nes-architect.fr",
			label: "www.nes-architect.fr"
		},

		twitter: {
			link: "https://twitter.com/nimaskji",
			label: "@nimaskji"
		},

		linkedin: {
			link: "https://fr.linkedin.com/in/thomas-depretz-50330a40",
			label: "Thomas Depretz"
		},

		lang: [
			{name: "fr", label: "French", level: 5},
			{name: "en", label: "English", level: 4}
		],

		hobbits: [
			"Mangas",
			"New technologies",
			"Cinema",
			"S.King (It, Christine)"
		]

	},

	/**
	* Define the content of resume text
	**/
	resume: "I am passionate about the new technologies of WEB and Systems. I like computing since small and continues to grow my skills in various fields. I decided to move towards cloud computing recently and therefore to devote myself to Amazon Web Services in order to become an expert and allow me to offer solutions and training on this technology.",

	/**
	* Define all pro experiences 
	**/
	exps: [

		{
			titlejob: "Production Engineer",
			enterprise: "Credit Agricole - Corporate & Investiment Bank for ANEO",
			dates: {start: "Avr. 2016", end: undefined},
			content: [
				"Deploy and maintain the grid middleware for CA-CIB with Singapour",
				"Implement grid middleware 'Symphony' from IBM for GaaS project",
				"Create JAVA API in .jar format to controll Datasynapse for DRP automation",
				"Create RestFULL API to translate Datasynaspe WSDL services and expose it to customers",
				"Create Datasynapse client to simulate workload on the grid",
				"Production support on Datasynapse middleware",
				"Discovering & formation on DevOps",
				"Formation on AWS : Amazon Web Services"
			]
		},
		{
			titlejob: "Founder of Nes-Architect",
			enterprise: "Thomas DEPREETZ (Nes-Architect)",
			dates: {start: "Sept. 2015", end: undefined},
			content: [
				"Migrating physical infrastructure from DC to OVH",
				"Upgrading virtual infra for HA on 3 DC on OVH with V-RACK solution",
				"Developping and hosting websites / web application",
				"Training BTS students to pass exams",
				"Migrating self service into AWS (started on Dec 2017)",
				"AWS Training to pass certifications",
				"Registered as APN (AWS Partner Network)"
			]
		},
		{
			titlejob: "Chief Information Officer",
			enterprise: "CFA INSTA",
			dates: {start: "Oct. 2014", end: "Avr. 2016"},
			content: [
				"CIO of System and architecture of the school",
				"Funder of the exam platform for BTS practice",
				"IT Manager of the Cloud computing bay",
				"Improved quality of life for students on the system & network"
			]
		},
		{
			titlejob: "System Administrator",
			enterprise: "CFA INSTA",
			dates: {start: "Dec. 2012", end: "Aug. 2014"},
			content: [
				"Replace old infrastructure by 10G hardware in the school.",
				"Install the front firewall and add filter solution for students.",
				"Virtualization with ESXi and Hyper-V"
			]
		},
		{
			titlejob: "Analyst & Developper",
			enterprise: "Advanced Partner",
			dates: {start: "Sept. 2011", end: "Sept. 2012"},
			content: [
				"Development of web app and in FLEX/AS3/PHP",
				"install/maintain St gobin infrastructure at Lyon",
				"Deploy dynamic display information system at St Gobain"
			]
		}

	],

	/**
	* Define all formations (studies)
	**/
	formations: [
		{
			title: "Technical Architect in System & Networks (RNCP II:29067)",
			dates: {start: "2013", end: "2014"},
			school: "CFA INSTA - Paris",
		},
		{
			title: "Analyst in System Engineering and Networks (RNCP I:29069)",
			dates: {start: "2012", end: "2013"},
			school: "CFA INSTA - Paris",
		},
		{
			title: "Network Security & telecomunication (LP)",
			dates: {start: "2011", end: "2012"},
			school: "Université d'Evry-Val Essonne",
		},
		{
			title: "Advanced Technician in Systems and Networks (BTS)",
			dates: {start: "2009", end: "2011"},
			school: "Lycée Parc de Vilgenis, Massy Palaiseau",
		},
		{
			title: "Microinformatics and networks, installation and maintenance (BAC P)",
			dates: {start: "2007", end: "2009"},
			school: "Lycée Léonard de Vinci, Bagneux",
		}
	],

	/**
	* List certifications
	* status codes :
	*  0 : started
	*  1 : obtained 
	**/
	certs : [
		{label: "AWS Cloud Practionner", code: "https://www.certmetrics.com/amazon/public/badge.aspx?i=9&t=c&d=2018-01-12&ci=AWS00401899",badge:"https://www.certmetrics.com/api/ob/image/amazon/c/9", status: 1, date: "12 Janv 2017"},
		{label: "AWS Soltion Architect : Associate", code: "", badge: "https://www.pjlewis.org/wp-content/uploads/2017/09/aws-solutions-architect-associate.png?x30801&x30801", status: 0, date: undefined}
	],

	/**
	* Define your skills
	**/
	skills : [
		{label: "Amazon Web Services", level: 2.0},
		{label: "Windows Family", level: 5},
		{label: "Linux Family", level: 4.5},
		{label: "Virt. VMWare", level: 3.5},
		{label: "Virt. Proxmox", level: 4.25},
		{label: "Dev&Ops", level: 3},
		{label: "NodeJS & VueJS", level: 3.25},
		{label: "PHP,HTML5 & CSS", level: 4.25},
		{label: "SGBDR MariaDB", level: 4.25},
		{label: "Scripting (PS, SH)", level: 4.5},
		{label: "Data Center", level: 2.5},
	],
}